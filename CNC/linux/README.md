# The CNC Linux images

## The directories and files

| Directory/File | Notes                                                                          |
| :------------- | :----------------------------------------------------------------------------- |
| files          | This directory has the files that are pushed to the image using packer         |
| - mvfiles.sh   | Script run by packer to copy files into place quickly from the files directory |
| roles          | This directory has the ansible roles that are used to build linux images       |
| images.json    | The packer json template for the linux images                                  |
| nitc-fy.yml    | Ansible playbook for building linux images                                     |
| str2pwhash.py  | Python script to convert plain text to a password hash                         |
