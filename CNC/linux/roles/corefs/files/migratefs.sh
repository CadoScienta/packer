#!/bin/bash

# This script will temporarily mount the new mount point and then
# copy the files from the directory into the mount point. It will
# then mount the new filesystem to the mount point and clean up
# the files underneath of it.  It is designed to work inside of a
# loop. That loop should have nested filesystems starting with the
# directory that is closet to /

VGNAME=$1
FS=$2
LVNAME=$3

mount /dev/mapper/${VGNAME}-${LVNAME} /mnt
cd ${FS}
cp -frdp * /mnt
mount ${FS}
umount /mnt
mount -o bind / /mnt
cd /mnt/${FS}
rm -rf *
cd /
umount /mnt
