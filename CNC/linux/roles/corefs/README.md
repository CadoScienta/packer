corefs
======

Builds out the core filesystems for a NITC build

Requirements
------------

Expects that there is a second disk available for the VG. Disk name varies based on platform.

Role Variables
--------------

The filesystems that are going to be built. If state is present, then it is assumed that the direcotry already exists. If thes state is mounted, then it is assumed that the directory is new.
```yaml
filesystems:
  -
    lvname: varlv
    name: /var
    size: 4g
    state: present
    type: xfs
```

Dependencies
------------

This is designed to work with packer, and uses variables that packer passes into the playbook to make platform decisions.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:
```yaml
- hosts: servers
  roles:
    - corefs
```

License
-------

BSD

Author Information
------------------

JD Runyan<br>
Jason.Runyan@USDA.gov
