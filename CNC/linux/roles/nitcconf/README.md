nitcconf
========

This will set the system up for use in the NITC datacenters

Requirements
------------

The corefs role or a similar FS layout should be applied before this role

Role Variables
--------------

All variable are set in defaults

Dependencies
------------

corefs should be applied before running this

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

```yaml
- hosts: servers
  roles:
    - nitcconf
```

License
-------

BSD

Author Information
------------------

JD Runyan<br>
Jason.Runyan@USDA.gov
