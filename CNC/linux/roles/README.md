# Ansible roles used for images

| Role        | Description                                                     |
| :---------- | :-------------------------------------------------------------- |
| cloudagents | Installs specific cloud platform agents                         |
| corefs      | Configures the core filesystems starting with a raw disk -> LVM |
| nitcconf    | Installs agents and hardens the system to NITC standards        |
