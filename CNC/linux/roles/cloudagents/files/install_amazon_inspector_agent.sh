#!/bin/bash

# Install the Amazon Inspector Agent
TEMPDIR=$( mktemp -d )
cd ${TEMPDIR}
curl -O https://inspector-agent.amazonaws.com/linux/latest/install
bash install
cd
rm -rf ${TEMPDIR}
