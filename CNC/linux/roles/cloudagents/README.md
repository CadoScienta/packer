cloudagents
===========

Installs agents for the cloud platform providers.

Requirements
------------

It is expected that this is called from packer, and packer variables about the platform are available.

Role Variables
--------------

There are no role variables

Dependencies
------------

No other ansible dependencies.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

```yaml
- hosts: servers
  roles:
    - cloudagents
```

License
-------

BSD

Author Information
------------------

JD Runyan<br>
Jason.Runyan@USDA.gov
