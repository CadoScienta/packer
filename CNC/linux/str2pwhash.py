#!/usr/bin/env python3

import string
import crypt
import sys

passtxt = sys.argv[1]

print(passtxt)
print(crypt.crypt(passtxt,crypt.mksalt(crypt.METHOD_SHA512)))

