This directory holds the files that are loaded to a web server to download
during the image creation process. WinRM is too slow to effectively transfer
the files to the systems while building the images. Once an image management
server is set up, these files will be hosted from the build directory, and
won't have to be loaded to another web server.

* nitcsoftware:                         The directory that is zipped into nitcsoftware.zip
* nitcsoftware.zip:                     The file that is downloaded to the image to install
* Win8.1AndW2K12R2-KB3191564-x64.msu:   Powershell 5.1 installation
