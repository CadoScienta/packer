# The CNC Windows images

## The directories and files

| Directory/File     | Notes                                                                          |
| :----------------- | :----------------------------------------------------------------------------- |
| connection-plugins | This directory has the connector for packer to use WinRM with Ansible          |
| files              | This directory has the files that are pushed to the image using packer         |
| ec2-userdata.ps1   | AWS user data script to turn on WinRM                                          |
| images.json        | The packer json template for the windows images                                |
| nitc-fy-p1.yml     | Ansible playbook for building windows images - Part 1                          |
| nitc-fy-p2.yml     | Ansible playbook for building windows images - Part 2                          |
