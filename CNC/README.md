# The CNC images for AWS and Azure

The current focus of the images are for AWS. Azure specific images are not being created, but
simply duplicates of the AWS images without the AWS specific tooling being added to them. They
will serve as the base for future use in an Azure CNC effort.

## The directories and files

| Directory/File | Notes                                                                       |
| :------------- | :-------------------------------------------------------------------------- |
| linux          | This directory has all the pieces needed to build linux CNC images          |
| windows        | This directory has all the pieces needed to build windows CNC images        |
