# Project to work on packer images for linux and windows

This project is for automating the creation of images to use in the cloud.
The images are based off of existing images provided by the cloud platforms,
and are then modified to be used by NITC and its customers. The images include
all agents and are as preconfigured as possible. They are also designed to be
at the current patch level as production systems.

## The directories and files

| Directory/File | Notes                                                                       |
| :------------- | :-------------------------------------------------------------------------- |
| CNC            | This directory is for images that are designed for using cloud native tools |
| .kateproject   | Sets up a project for the Kate IDE, only useful if you are using Kate       |
| prep.sh        | Has variables set for environment to build properly                         |

## The following environment will need to be set for the images to build
  - prep.sh assumes that there is a hidden file ".prep.sh" in the same directory

| Variable              | Notes                                                             |
| :-------------------- | :---------------------------------------------------------------- |
| AWS_ACCESS_KEY_ID     | Access key that has permissions to build EC2 instances and AMIs   |
| AWS_SECRET_ACCESS_KEY | The secret that matches AWS_ACCESS_KEY_ID                         |
| AZURE_CLIENT_ID       | Azure client ID that has permissions to build VMs and save images |
| AZURE_CLIENT_SECRET   | The secret that matches AZURE_CLIENT_ID                           |
