#!/usr/bin/env python

import yaml
import datetime
import jinja2

# Import yaml file
try:
    with open('packervars.yml') as f:
        dataMap = yaml.safe_load(f)
except:
    print("Unable to load packervars.yml")
    exit(10)

# Get the top keys as their own variables
locals().update(dataMap)

# Check to see if a IMAGE_NAME_DEFAULT has been set
try:
    IMAGE_NAME_DEFAULT
except NameError:
    IMAGE_NAME_DEFAULT=UNNAMED
    print("No IMAGE_NAME_DEFAULT given. Using " + IMAGE_NAME_DEFAULT)

# Get date stamp for IMAGE_NAME
DT=datetime.datetime.now().strftime("%Y%m%d%H%M")

# Load jinja2 template
templateLoader = jinja2.FileSystemLoader(searchpath="./templates")
templateEnv = jinja2.Environment(loader=templateLoader)
TEMPLATE_FILE = "images.json.builders.j2"
try:
    template = templateEnv.get_template(TEMPLATE_FILE)
except:
    print("Problem with template file " + TEMPLATE_FILE)
    exit(20)

# Loop through defined environments
packerFile = ""
for environment in ENVIRONMENTS.keys():
    print('ENVIRONMENT: ' + environment)
    J2VARS=ENVIRONMENTS.get(environment)
    J2VARS['NAME'] = environment
    try:
        IMAGE_NAME = J2VARS.get('IMAGE_NAME',IMAGE_NAME_DEFAULT) + "-" + DT
    except:
        pass
    J2VARS['IMAGE_NAME'] = IMAGE_NAME
    packerFile = packerFile + template.render(dict=J2VARS)

# Write to file
FILENAME = "./images-" + DT + ".json"
header = open("./templates/images.json.header")
footer = open("./templates/images.json.footer")
newfile = open(FILENAME,"w")
for line in header:
    newfile.write(line)
newfile.write("\t\"builders\":\n\t[" + packerFile[:-1] + "\n\t],\n")
for line in footer:
    newfile.write(line)
