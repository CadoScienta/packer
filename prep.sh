#!/usr/bin/env bash

packer -autocomplete-install

# import IDs and secrets
. ./.prep.sh

# Values shared between AWS and Azure
export IMAGE_NAME="NITC-Linux-RHEL7"
export ENVIRONMENT_TAG="PRD"

# AWS specifc values
export AWS_REGION="us-gov-west-1"
export AWS_INSTANCE_TYPE="t2.medium"

export AWS_VPC_ID="vpc-6a4af40f"
export AWS_SUBNET_ID="subnet-62c8c407"
export AWS_SSH_USERNAME="ec2-user"
export AWS_WINRM_USERNAME="Administrator"

# Azure specific values
export AZURE_CLOUD_ENVIRONMENT_NAME="USGovernment"
export AZURE_LOCATION="USGov Virginia"
export AZURE_VM_SIZE="Standard_D2s_v3"

export AZURE_SUBSCRIPTION_ID="b85cdb8b-3213-4cce-8e15-1a89c2b382d7"
export AZURE_TENANT_ID="0ea120e1-27fd-4a98-8e78-7c83eb17960c"
export AZURE_VIRTUAL_NETWORK_NAME="AZUG-VA-OIR-VNTHUB"
export AZURE_VIRTUAL_NETWORK_SUBNET_NAME="AZUG-VA-OIR-GSSZ-001"
export AZURE_VIRTUAL_NETWORK_RESOURCE_GROUP_NAME="AZUG-OIR-SHARED-RG001"
export AZURE_MANAGED_IMAGE_RESOURCE_GROUP_NAME="AZUG-OIR-SHARED-RG001"
export AZURE_WINRM_USERNAME="Administrator"
